<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package plasterdog-hero
 */

get_header(); ?>
<div id="header-bump"></div>
<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<h1><?php the_title(); ?></h1>
</div>
<?php endif; ?>	

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'plasterdog-hero' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>

			<?php plasterdog_hero_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>
<div class="clear"><hr/></div>
		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
