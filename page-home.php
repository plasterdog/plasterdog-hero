<?php
/**
 * Template Name: Landing Page
 *
 *
 * @package plasterdog-hero
 */

get_header(); ?>
	
<div id="header-bump"></div>
<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<h1><?php the_title(); ?></h1>
</div>
<?php endif; ?>	

<?php if (! get_field( 'page_hero_image' ) ): ?>
<div id="big-header-bump"></div>
<?php endif; ?>	

<!-- THREE FEATURED ITEMS ABOVE CONTENT REGION -->
<?php if( get_field('featured_items_position') == 'above-content' ): ?>
			<!--THE THREE FEATURED ITEMS -->
		
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item">						
						<div class="feature-icon">
						<a href="<?php the_field('first_feature_link'); ?>"><img src="<?php the_field('first_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('first_feature_link'); ?>"><h3><?php the_field('first_feature_title'); ?> </h3></a>
						<p><?php the_field('first_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('first_feature_link'); ?>">Find out more</a></p>						
						</div><!-- ends feature content -->

					</div>
					</li>
					<li>
					<div class="inner-focus-item">							
						<div class="feature-icon" >
						<a href="<?php the_field('second_feature_link'); ?>"><img src="<?php the_field('second_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('second_feature_link'); ?>"><h3><?php the_field('second_feature_title'); ?> </h3></a>
						<p><?php the_field('second_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('second_feature_link'); ?>">Find out more</a></p>						
						</div><!-- ends feature content -->

					</div>
					</li>
					<li>		
					<div class="inner-focus-item">						
						<div class="feature-icon">
						<a href="<?php the_field('third_feature_link'); ?>"><img src="<?php the_field('third_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('third_feature_link'); ?>"><h3><?php the_field('third_feature_title'); ?> </h3></a>
						<p><?php the_field('third_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('third_feature_link'); ?>">Find out more</a></p>						
						</div><!-- ends feature content -->

					</div>	
					</li>			
				</ul>
<!-- making the region conditional based on entry -->
<?php if ( $post->post_content!=="" ) {		?>	
		<!--THE CONTENT-->
<div id="page" class="hfeed site"  >
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area">
				<main id="main" class="full-site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>	
				</main><!-- #main -->
			</div><!-- #primary -->
			</div><!-- #content -->
		</div><!-- #page -->

		<div class="clear" style="height:2em;"></div>

<?php } ?><!-- ending the condition -->

<?php endif; ?><!-- the radio button clause -->


<!-- THREE FEATURED ITEMS BELOW CONTENT REGION -->
<?php if( get_field('featured_items_position') == 'below-content' ): ?>
<!-- making the region conditional based on entry -->
<?php if ( $post->post_content!=="" ) {		?>		
		<!--THE CONTENT-->
		<div id="page" class="hfeed site" style="margin-top:3em;">
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area" >
				<main id="main" class="full-site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>	
				</main><!-- #main -->
			</div><!-- #primary -->
			</div><!-- #content -->
			</div><!-- #page -->
			<div class="clear"></div>
			<?php } ?><!-- ending the condition -->
		
		<!--THE THREE ITEMS-->
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item">
						
						<div class="feature-icon">
						<a href="<?php the_field('first_feature_link'); ?>"><img src="<?php the_field('first_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('first_feature_link'); ?>"><h3><?php the_field('first_feature_title'); ?> </h3></a>
						<p><?php the_field('first_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('first_feature_link'); ?>">Find out more</a></p>							
						</div><!-- ends feature content -->

					</div>
					</li>
					<li>
					<div class="inner-focus-item">	
						<div class="feature-icon" >
						<a href="<?php the_field('second_feature_link'); ?>"><img src="<?php the_field('second_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('second_feature_link'); ?>"><h3><?php the_field('second_feature_title'); ?> </h3></a>
						<p><?php the_field('second_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('second_feature_link'); ?>">Find out more</a></p>							
						</div><!-- ends feature content -->

						</div>
					</li>
					<li>		
					<div class="inner-focus-item">
						<div class="feature-icon">
						<a href="<?php the_field('third_feature_link'); ?>"><img src="<?php the_field('third_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('third_feature_link'); ?>"><h3><?php the_field('third_feature_title'); ?> </h3></a>
						<p><?php the_field('third_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('third_feature_link'); ?>">Find out more</a></p>							
						</div><!-- ends feature content -->

					</div>	
					</li>			
				</ul>

<div class="clear" style="height:2em;"></div>
<?php endif; ?>
<?php get_footer(); ?>
