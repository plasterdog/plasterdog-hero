<?php
/**
 * Template Name: Split Page
 *
 *
 * @package plasterdog-hero
 */

get_header(); ?>
<div id="header-bump"></div>
<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>
<h1><?php the_title(); ?></h1>	
</div>
<?php endif; ?>	

<?php if (! get_field( 'page_hero_image' ) ): ?>
<div id="big-header-bump"></div>
<?php endif; ?>	

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				
			</header><!-- .entry-header -->

			<div class="entry-content">
					<?php if ( !get_field( 'page_hero_image' ) ): ?>
	<h1><?php the_title(); ?></h1>	
	<?php endif; ?>	
				<?php if(get_field('post_author_name')) {?>
  <small>written by: <?php the_field('post_author_name'); ?></small>
  <hr/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('post_author_name')) {?>
    
<?php }?> <!-- ends the second outer condition -->  
				<?php the_content(); ?>
				<div class="left-side">
				<?php the_field('left_section'); ?>	
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'plasterdog-hero' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- ends left side -->
			<div class="right-side">
				<?php the_field('right_section'); ?>
				
			</div><!-- ends right side-->
			</div><!-- .entry-content -->
	

	<?php edit_post_link( __( 'Edit', 'plasterdog-hero' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
